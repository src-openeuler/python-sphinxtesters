%global _empty_manifest_terminate_build 0

Name:		python-sphinxtesters
Version:	0.2.3
Release:	1
Summary:	Utilities for testing Sphinx extensions
License:	BSD
URL:		http://github.com/matthew-brett/sphinxtesters
Source0:	https://files.pythonhosted.org/packages/23/35/1c4d8d18ab8585063c65d8aa5abcc3ca2a68b852a473fa98d773f9e43c8e/sphinxtesters-0.2.3.tar.gz
BuildArch:	noarch


%description
Sphinxtesters - utilities for testing Sphinx extensions

%package -n python3-sphinxtesters
Summary:	Utilities for testing Sphinx extensions
Provides:	python-sphinxtesters
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%description -n python3-sphinxtesters
Sphinxtesters - utilities for testing Sphinx extensions

%package help
Summary:	Development documents and examples for sphinxtesters
Provides:	python3-sphinxtesters-doc

%description help
Sphinxtesters - utilities for testing Sphinx extensions

%prep
%autosetup -n sphinxtesters-0.2.3

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-sphinxtesters -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Aug 05 2021 Wang Shuo <wangshuo@kylinos.cn> - 0.2.3-1
- Init package in openeuler

* Thu Aug 05 2021 Python_Bot <Python_Bot@openeuler.org> - 0.2.3-1
- Package Spec generated
